MarcosWeb::Application.routes.draw do
  root to: 'home#index'

  resources :proyects, only: [:index]
  resources :contacts, only: [:index,:create]
  resources :abouts, only: [:index]
  resources :blogs, only: [:index,:show] do
    resources :comments, only: [:create]
  end

  devise_for :users,skip: [:registrations,:passwords], path: "auth",
      path_names:
      { sign_in: 'login', sign_out: 'logout', password: 'secret',
        confirmation: 'verification', unlock: 'unblock',
        registration: 'register', sign_up: 'cmon_let_me_in'
      }
  as :user do
    get 'admin/users/edit' => 'devise/registrations#edit', :as => 'edit_user_registration'
    put 'admin/users' => 'devise/registrations#update', :as => 'user_registration'
  end



  namespace :admin do
    match 'home' =>'home#index',as: 'home', via: [:get]
    resources :proyects ,except: [:show] do
      resources :pphotos, only: [:create, :destroy]
    end
    resources :posts,except: [:show] do
      collection do
        post 'search' => 'posts#search', as: 'search'
        get 'sort/:by' => 'posts#sort', as: 'sort'
      end
    end
    resources :cvs,only: [:edit, :update]
    resources :comments,except: [:show, :new,:create] do
      collection do
        post 'search' => 'comments#search', as: 'search'
      end
    end


  end

end


