#RUTA DONDE ESTA INSTALADO PARA PUESTA EN PRODUCCION
ENV['MM_ROUTE_INSTALLED']='/home/foo/foo/marcos_web'

#CONSTANTES PARA EL MAIL
ENV['MM_MAIL_API_KEY']='foo'

#CONSTANTES PARA LA BASE DE DATOS
if Rails.env.test?
  ENV['MM_MYSQL_DATABASE']='marcos_web_test'
elsif Rails.env.development?
  ENV['MM_MYSQL_DATABASE']='marcos_web_dev'
elsif Rails.env.production?
  ENV['MM_MYSQL_DATABASE']='marcos_web'
end
ENV['MM_MYSQL_USER']='foo'
ENV['MM_MYSQL_PASSWORD']='foo'
ENV['MM_MYSQL_PORT_3306_TCP_ADDR']='foo'