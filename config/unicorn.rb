worker_processes 2

APP_PATH = '/home/marcos/webs/marcos_web'
working_directory APP_PATH # available in 0.94.0+

listen APP_PATH + '/tmp/sockets/unicorn.app1.sock', backlog: 64
listen 8080, tcp_nopush: true

timeout 30

pid APP_PATH + '/tmp/pids/unicorn1.pid'

stderr_path APP_PATH + '/log/unicorn1.stderr.log'
stdout_path APP_PATH + '/log/unicorn1.stdout.log'
