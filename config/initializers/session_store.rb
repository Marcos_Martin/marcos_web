# Be sure to restart your server when you modify this file.

#MarcosWeb::Application.config.session_store :cookie_store, key: '_marcos_web_session'
MarcosWeb::Application.config.session_store ActionDispatch::Session::CacheStore, expire_after: 60.minutes

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rails generate session_migration")
# MarcosWeb::Application.config.session_store :active_record_store
