source 'https://rubygems.org'

gem 'rails', '3.2.14'
#gem 'jquery-rails'
gem 'cloudfoundry-jquery-rails', '~> 1.0.19.1'

# Use mysql for database
gem 'mysql2', '~> 0.3.18'

# Use unicorn as the app server for Unix
gem 'unicorn-rails','~> 1.1.0'

# authenticate for rails
gem 'devise', '~> 1.5.0'

# permisions for authenticates
gem 'cancan', '~> 1.6.007'

# To use ActiveModel has_secure_password
gem 'bcrypt-ruby', '~> 3.0.0'

# mini office for bootstrap Twitter
gem 'bootstrap-wysihtml5-rails','~> 0.3.1.24'

# mini office mas completo
gem 'tinymce-rails','~> 4.0.12'
gem 'tinymce-rails-langs'
gem 'ckeditor','~> 4.0.8'

# paginate tables in views
gem 'will_paginate', '~> 3.0.5'

# display images
gem 'colorbox-rails', '~> 0.1.0'

# friendly id
gem 'friendly_id', '~> 4.0.10.1'

# preguntas para formularios anti robots
gem 'humanizer', '~> 2.6.2'

# quitar fragmentos HTML y CSS de una cadena
gem 'sanitize', '~> 3.0.4'

# mas iconitos :)  http://fortawesome.github.io/Font-Awesome/icons/
gem 'font-awesome-sass', '~> 4.4.0'

# integracion con email de mailgun
gem 'mailgun-ruby', '~>1.1.10'

# Cache por Dalli
gem 'dalli', '~>2.7.9'

###################################################################

group :assets do
  # Twitter Bootstrap 3.0.3.0 (https://github.com/anjlab/bootstrap-rails)
  gem 'anjlab-bootstrap-rails', '~> 3.0.3.0', require: 'bootstrap-rails'

  # integration with the Sass stylesheet language
  gem 'sass-rails','~> 3.2.6'

  # Embed the V8 JavaScript interpreter into Ruby.
  gem 'therubyracer','~> 0.12.0' ,platforms: :ruby

  # JavaScript compressor
  gem 'uglifier','~> 2.3.2'

  # Aceleracion al generar el contenido estatico para producion
  #gem 'turbo-sprockets-rails3'
end

group :development do
  # Ayuda exta en la consola de rails.
  # Usa binding.pry para hacer break en la ejecucion y poder depurar variables
  # Usa table User.all para mostrar tablas en modo grafico por consola
  gem 'awesome_rails_console', '~> 0.4.0'
  gem 'hirb', '~> 0.7.3'
  gem 'hirb-unicode', '~> 0.0.5'
  gem 'pry-byebug', '~> 3.4.2'
  gem 'pry-stack_explorer', '~> 0.4.9.2'

  # replaces the standard Rails error page with a much useful error page
  gem 'better_errors','~> 1.0.1'

  # see partials in views with ctrl+shift+x
  gem 'xray-rails','~> 0.1.10'

  # see routes in you app like 'rake:routes' with: http://localhost:3000/rails/routes
  gem 'sextant', '~> 0.2.4'

  # Preview email in the browser instead of sending it.
  # Set the delivery method in config/environments/development.rb
  # config.action_mailer.delivery_method = :letter_opener
  # Now any email will pop up in your browser instead of being sent. The messages are stored in tmp/letter_opener
  gem 'letter_opener','~> 1.1.2'

  # generate fake datas in database
  gem 'faker','~> 1.2.0'

end

group :test do
  # testing framework
  gem 'rspec','~> 2.14.1'
  # test simulation like a real user. Can be used with rspec
  gem 'capybara','~> 2.2.0'
end
