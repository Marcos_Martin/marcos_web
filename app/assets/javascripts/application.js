//= require jquery
//= require jquery_ujs
//= require tinymce

// Include all bootstrap javascripts
//= require twitter/bootstrap

// Or peek any of them yourself
//= require twitter/bootstrap/transition
//= require twitter/bootstrap/alert
// require twitter/bootstrap/modal
// require twitter/bootstrap/dropdown
// require twitter/bootstrap/scrollspy
// require twitter/bootstrap/tab
// require twitter/bootstrap/tooltip
// require twitter/bootstrap/popover
//= require twitter/bootstrap/button
// require twitter/bootstrap/collapse
//= require twitter/bootstrap/carousel
//= require twitter/bootstrap/affix
//= require colorbox-rails
//= require bootstrap-wysihtml5/b3
//= require bootstrap-wysihtml5/locales/es-ES
//= require ckeditor/init
//= require_tree .


function run_rich_text_editor(languague){
    $(document).ready(function(){

        $('.wysihtml5').each(function(i, elem) {
            $(elem).wysihtml5({
                locale: languague,
                "font-styles": true, //Font styling, e.g. h1, h2, etc. Default true
                "emphasis": true, //Italics, bold, etc. Default true
                "lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
                "html": true, //Button which allows you to edit the generated HTML. Default false
                "link": true, //Button to insert a link. Default true
                "image": true, //Button to insert an image. Default true,
                "color": true //Button to change color of font
            });
        });

    })
}

function run_rich_text_editor_small(languague){
    $(document).ready(function(){

        $('.wysihtml5').each(function(i, elem) {
            $(elem).wysihtml5({
                locale: languague,
                "font-styles": true, //Font styling, e.g. h1, h2, etc. Default true
                "emphasis": true, //Italics, bold, etc. Default true
                "lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
                "html": false, //Button which allows you to edit the generated HTML. Default false
                "link": true, //Button to insert a link. Default true
                "image": false, //Button to insert an image. Default true,
                "color": true //Button to change color of font
            });
        });

    })
}

function colorbox_galleries(num_proyects){
    $(document).ready(function(){
        for(var i=0;i<num_proyects;i++){
        $(".group"+i).colorbox({rel:'group'+i, transition:"fade", width:"90%", height:"90%"});
        }
    })
}

function start_carrousel(){
    $(document).ready(function(){
        $('.carousel').carousel({
            interval: 9999
        });
    });
}

