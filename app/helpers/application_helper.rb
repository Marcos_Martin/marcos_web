module ApplicationHelper
  def set_active_home
    params[:controller] == 'home' ? 'class=active' : ''
  end
  def set_active_proyects
    params[:controller] == 'proyects' ? 'class=active' : ''
  end
  def set_active_about_me
    params[:controller] == 'abouts' ? 'class=active' : ''
  end
  def set_active_blog
    params[:controller] == 'blogs' ? 'class=active' : ''
  end
  def set_active_contact
    params[:controller] == 'contacts' ? 'class=active' : ''
  end
  def get_kind_proyects
    ['rails','java','c++','php','wordpress','photoshop','hardware']
  end

end
