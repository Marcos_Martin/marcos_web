#Dar roles a Cancan para poder interactuar con Devise
class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new
    if user.is_administrator?
      can :manage, :all
    elsif user.is_invited?
      can :read,:all
    end
  end

end
