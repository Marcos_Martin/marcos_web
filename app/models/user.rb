class User < ActiveRecord::Base
  # :token_authenticatable, :encryptable, :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :recoverable, :rememberable,
         :trackable, :validatable, :registerable

  attr_accessible :email, :password, :password_confirmation, :remember_me,
                  :id,:name, :alias,:occupation,:about_me,:cp,:media,:country_id,
                  :province_id,:locality,:username,:active

  ADMIN_ROLE = "admin"
  INVITED_ROLE = "invited"

  has_and_belongs_to_many :roles
  before_destroy { role.clear }

  def has_role?(role)
    return !!self.roles.find_by_name(role.to_s)
  end
  def is_administrator?
    has_role? ADMIN_ROLE
  end
  def is_invited?
    has_role? INVITED_ROLE
  end

end
