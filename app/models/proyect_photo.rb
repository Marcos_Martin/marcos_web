class ProyectPhoto < ActiveRecord::Base
  attr_accessible :media
  validates_presence_of :media

  extend FriendlyId
  friendly_id :id, use: :slugged

  belongs_to :proyect

end
