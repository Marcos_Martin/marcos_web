#encoding: utf-8
class Message
  include ActiveModel::Validations
  include ActiveModel::Conversion
  extend ActiveModel::Naming
  include Humanizer
  require_human_on :all

  attr_accessor :name,:surname, :email, :subject, :body,:phone,:file,:humanizer_answer,:humanizer_question_id
  validates_presence_of :name,:surname, :email, :subject, :body
  validates_format_of :email, with: /^(|(([A-Za-z0-9]+_+)|([A-Za-z0-9]+\-+)|([A-Za-z0-9]+\.+)|([A-Za-z0-9]+\++))*[A-Za-z0-9]+@((\w+\-+)|(\w+\.))*\w{1,63}\.[a-zA-Z]{2,6})$/i
  validate :validates_uploadfile

  def validates_uploadfile()
    max_size = 20971520  #bytes de 20Mbytes (correo de gmail max=25Mbytes)
    #177891bytes =173KB de windows
    if(file != nil)
      errors.add(:archivo, t('activerecord.attributes.message.file_overload')) if file.size > max_size
    end

  end

  def initialize(attributes = {})
    attributes.each do |name, value|
      send("#{name}=", value)
    end
  end

  def persisted?
    false
  end

end
