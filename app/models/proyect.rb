class Proyect < ActiveRecord::Base
  attr_accessible :name, :description,:sort_description, :media , :languague
  validates_presence_of :name,:description,:sort_description,:languague,:media

  extend FriendlyId
  friendly_id :name, use: :slugged

  has_many :proyect_photos,dependent: :destroy

  PAGE=15
  PAGE_INDEX_PUBLIC=8
end
