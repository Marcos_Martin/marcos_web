class Comment < ActiveRecord::Base
  include Humanizer
  require_human_on :create
  # Es posible crear comentarios para cualquier objeto
  attr_accessible :body, :email, :user,:humanizer_answer,:humanizer_question_id
  validates_presence_of :body, :email,:user
  validates_format_of :email, with: /^(|(([A-Za-z0-9]+_+)|([A-Za-z0-9]+\-+)|([A-Za-z0-9]+\.+)|([A-Za-z0-9]+\++))*[A-Za-z0-9]+@((\w+\-+)|(\w+\.))*\w{1,63}\.[a-zA-Z]{2,6})$/i
  belongs_to :commentable, polymorphic: true

  #replicas a los comentarios desactivados en modelo y migracion
  #has_many :replies, class_name: :Comment, foreign_key: :replied_of_id, dependent: :destroy

  extend FriendlyId
  friendly_id :id, use: :slugged

  PAGE_INDEX=20
end
