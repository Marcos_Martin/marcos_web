class Post < ActiveRecord::Base
  attr_accessible :body, :title, :media, :draft
  validates_presence_of :body, :title
  has_many :comments, as: :commentable, dependent: :destroy

  extend FriendlyId
  friendly_id :title, use: :slugged

  PAGE_INDEX=10
  PAGE_INDEX_PUBLIC=3

  scope :drafts, -> { where(draft: true).order(['created_at DESC']) } #mostrar borradores
  scope :actives, -> {where(draft: false).order(['created_at DESC'])} #mostrar activos


  def self.years
    select("created_at").order('created_at DESC').map{ |item| item.created_at.year }.uniq
  end
  def self.by_year_and_month(year,month)
    where("YEAR(created_at) =#{year}").where("MONTH(created_at) = #{month}")
  end
  def self.by_year(year)
    where("YEAR(created_at) =#{year}")
  end
end
