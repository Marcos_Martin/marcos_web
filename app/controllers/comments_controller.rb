class CommentsController < ApplicationController
  def create
    @comment = Comment.new(params[:comment])
    #de momento solo tendre comentarios para Blog
    @comment.commentable_id = Post.find_by_slug(params[:blog_id]).id
    @comment.commentable_type = 'Post'
    @message = Message.new(name: @comment.user,
                           surname: '_',
                           email: @comment.email,
                           subject: @url+'blogs/'+@comment.commentable.slug,
                           body: @comment.body,
                           phone: '_')
    respond_to do |format|
      if @comment.save
        CommentNotifier.contact_me(@message).deliver
        session[:post]=''
        format.html { redirect_to :back, notice: t('activerecord.attributes.comment.create') }
        format.json { render json: @comment, status: :created, location: @comment }
      else
        errors = humanize_errors_html(@comment.errors)
        session[:post]=@comment.body
        format.html { redirect_to :back, alert: t('activerecord.attributes.comment.fail')+errors }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end
end
