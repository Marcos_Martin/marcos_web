class Admin::CommentsController < ApplicationController
  authorize_resource class: false

  def index
    @comments=Comment.paginate(page: params[:page], per_page: Comment::PAGE_INDEX)
    respond_to do |format|
      format.html
      format.json { render json: @comments }
    end
  end

  def edit
    @comment = Comment.find(params[:id])
    @load_javascript="run_rich_text_editor('es-ES');"
  end

  def update
    @comment = Comment.find(params[:id])
    respond_to do |format|
      if @comment.update_attributes(params[:comment])
        format.html { redirect_to admin_comments_path, notice: t('activerecord.attributes.comment.update') }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @comment = Comment.find(params[:id])
    @comment.destroy
    flash[:alert]= t('activerecord.attributes.comment.remove')
    respond_to do |format|
      format.html { redirect_to  admin_comments_path  }
      format.json { head :no_content }
    end
  end

  def search
    @comments = Comment.paginate conditions: ['user LIKE ? and body LIKE ?',
                                          "%#{params[:search][:user]}%",
                                          "%#{params[:search][:body]}%"],
                             page: params[:page],
                             order: 'created_at desc',
                             per_page: Comment::PAGE_INDEX
    render "index"
  end
end
