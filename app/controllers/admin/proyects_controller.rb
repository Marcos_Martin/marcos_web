class Admin::ProyectsController < ApplicationController
  authorize_resource class: false

  def index
    @proyects = Proyect.all
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @proyects }
    end
  end

  def new
    @proyect = Proyect.new
    @load_javascript="run_rich_text_editor('es-ES');"
    respond_to do |format|
      format.html # new.html.erb.erb
      format.json { render json: @proyect }
    end
  end

  def edit
    @proyect = Proyect.find(params[:id])
    @proyect_photo = ProyectPhoto.new
    @proyect_photos= @proyect.proyect_photos
    @load_javascript="run_rich_text_editor('es-ES');"
  end

  def create
    @proyect = Proyect.new(params[:proyect])
    respond_to do |format|
      if @proyect.save
        format.html { redirect_to  edit_admin_proyect_path(@proyect), notice: t('activerecord.attributes.proyect.add') }
        format.json { render json: @proyect, status: :created, location: @proyect }
      else
        format.html { render action: "new" }
        format.json { render json: @proyect.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @proyect = Proyect.find(params[:id])
    respond_to do |format|
      if @proyect.update_attributes(params[:proyect])
        format.html { redirect_to admin_proyects_path, notice: t('activerecord.attributes.proyect.update') }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @proyect.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @proyect = Proyect.find(params[:id])
    @proyect.destroy
    flash[:alert]= t('activerecord.attributes.proyect.remove')
    respond_to do |format|
      format.html { redirect_to  admin_proyects_path  }
      format.json { head :no_content }
    end
  end
end
