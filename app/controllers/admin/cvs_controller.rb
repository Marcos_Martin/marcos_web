class Admin::CvsController < ApplicationController
  authorize_resource class: false

  def edit
    @cv = Cv.first
  end

  def update
    @cv = Cv.find(params[:id])
    respond_to do |format|
      if @cv.update_attributes(params[:cv])
        format.html { redirect_to edit_admin_cv_path(@cv), notice: t('activerecord.attributes.cv.update') }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @cv.errors, status: :unprocessable_entity }
      end
    end
  end

end
