class Admin::PostsController < ApplicationController
  authorize_resource class: false

  def index
    @posts=Post.paginate(page: params[:page], per_page: Post::PAGE_INDEX)
    respond_to do |format|
      format.html
      format.json { render json: @posts }
    end
  end

  def new
    @post=Post.new
    #@load_javascript="run_rich_text_editor('es-ES');"
    respond_to do |format|
      format.html
      format.json { render json: @post }
    end
  end

  def edit
    @post = Post.find(params[:id])
    #@load_javascript="run_rich_text_editor('es-ES');"
  end

  def update
    @post = Post.find(params[:id])
    respond_to do |format|
      if @post.update_attributes(params[:post])
        format.html { redirect_to admin_posts_path, notice: t('activerecord.attributes.post.update') }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  def create
    @post = Post.new(params[:post])
    respond_to do |format|
      if @post.save
        format.html { redirect_to  edit_admin_post_path(@post), notice: t('activerecord.attributes.post.add') }
        format.json { render json: @post, status: :created, location: @post }
      else
        format.html { render action: "new" }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @post = Post.find(params[:id])
    @post.destroy
    flash[:alert]= t('activerecord.attributes.post.remove')
    respond_to do |format|
      format.html { redirect_to  admin_posts_path  }
      format.json { head :no_content }
    end
  end

  def sort
    case params[:by]
      when 'draf_posts'
        @posts=Post.drafts.paginate(page: params[:page], per_page: Post::PAGE_INDEX).order('created_at DESC')
      else
        @posts=Post.all.paginate(page: params[:page], per_page: Post::PAGE_INDEX_PUBLIC).order('updated_at DESC')
    end
    render "index"
  end

  def search
    @posts = Post.paginate conditions: ['title LIKE ?',
                                              "%#{params[:search][:title]}%"],
                                 page: params[:page],
                                 order: 'created_at desc',
                                 per_page: Comment::PAGE_INDEX
    render "index"
  end
end
