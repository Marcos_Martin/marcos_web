class Admin::PphotosController < ApplicationController
  authorize_resource class: false

  def create
    @proyect_photo = ProyectPhoto.new(params[:proyect_photo])
    @proyect_photo.proyect_id=Proyect.find_by_slug(params[:proyect_id]).id

    respond_to do |format|
      if @proyect_photo.save
        format.html { redirect_to edit_admin_proyect_path(@proyect_photo.proyect), notice: t('activerecord.attributes.proyect_photo.create') }
        format.json { render json: @proyect_photo, status: :created, location: @proyect_photo }
      else
        format.html { redirect_to edit_admin_proyect_path(@proyect_photo.proyect) }
        format.json { render json: @proyect_photo.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @proyect_photo = ProyectPhoto.find(params[:id])
    @proyect_photo.destroy

    respond_to do |format|
      format.html { redirect_to edit_admin_proyect_path(@proyect_photo.proyect) }
      format.json { head :no_content }
    end
  end
end
