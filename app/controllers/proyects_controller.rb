class ProyectsController < ApplicationController
  before_filter :load_meta
  def index
    @proyects=Proyect.paginate(page: params[:page],order: 'created_at DESC', per_page: Proyect::PAGE_INDEX_PUBLIC)
    @load_javascript="colorbox_galleries(#{@proyects.count})"
  end


  private
  def load_meta
    @web_title= t('proyects.title')
    @description= t('proyects.description')
  end
end
