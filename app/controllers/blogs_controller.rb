# encoding: UTF-8
class BlogsController < ApplicationController
  before_filter :load_meta
  def index
    @posts=Post.actives.paginate(page: params[:page], per_page: Post::PAGE_INDEX_PUBLIC).order('updated_at DESC')
    @comment= Comment.new
    @load_javascript="run_rich_text_editor_small('es-ES');"
    respond_to do |format|
      format.html
      format.json { render json: @posts }
    end
  end

  def show
    @post = Post.find(params[:id])
    @load_javascript="run_rich_text_editor_small('es-ES');"
    @web_title="Marcos Martín Web - #{@post.title.upcase}"
    @url+= 'blogs/'+@post.slug
    @description=@post.title.upcase
    @description_fb= @description+': '+@url
        respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @post }
    end
  end

  private
  def load_meta
    @web_title= t('blog.title')
    @description= t('blog.description')
  end
end
