#encoding: utf-8
class ApplicationController < ActionController::Base
  protect_from_forgery
  before_filter :expire_hsts #forzar no httpS
  before_filter :web_default_meta
  before_filter :invited_permissions

  rescue_from CanCan::AccessDenied do |exception|
    flash[:notice] = t('errors.messages.not_allow')
    session[:user_return_to] = request.url
    redirect_to root_path
  end

  def after_sign_in_path_for(user)
      if user.is_administrator? or user.is_invited?
        admin_home_path
      else
        root_path
      end
  end


  private
  def web_default_meta
    params[:controller] == 'devise/sessions'?
        @web_title="Marcos Martín Web - #{t('devise.links.sign_in')}" :
        @web_title='Marcos Martín Web'
    @description= t('home.description')
    @url= 'http://marcosmartingm.es/'
    @image= 'assets/tux.png'
    @description_fb=@description
  end

  def invited_permissions
    if current_user.present? and current_user.is_invited? and
       params[:controller] == 'devise/registrations'
      raise CanCan::AccessDenied
    end
  end

  def humanize_errors_html(errors)
    aux_errors='<br/><ul>'
    errors.full_messages.each do |msg|
      aux_errors << '<li>'+msg+'</li>'
    end
    aux_errors << '</ul>'
    aux_errors
  end

  def expire_hsts
    response.headers["Strict-Transport-Security"] = 'max-age=0'
  end

end
