class AboutsController < ApplicationController
  before_filter :load_meta

  def index
    @cv=Cv.first
  end

  private
  def load_meta
    @web_title= t('about.title')
    @description= t('about.description')
  end
end
