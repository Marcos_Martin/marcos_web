class ContactsController < ApplicationController
  before_filter :load_meta
  before_filter :check_humanizer_answer, :only => [:create]
  def index
    @message = Message.new
  end
  def create
    @message = Message.new(params[:message])
    if @message.valid?
      ContactNotifier.contact_me(@message).deliver
      redirect_to(contacts_path, :notice => "El mensaje se envio correctamente.")
    else
      flash[:alert] = "Revise los campos, no se envio"
      render :index
    end
  end

  private
  def load_meta
    @web_title= t('contact.title')
    @description= t('contact.description')
  end
end

