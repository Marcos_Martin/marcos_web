class HomeController < ApplicationController
  before_filter :load_meta
  def index
    @carrusel=Proyect.order("RAND()").first(5)
    @load_javascript='start_carrousel();'
  end

  private
  def load_meta
    @web_title= t('home.title')
    @description= t('home.description')
  end
end
