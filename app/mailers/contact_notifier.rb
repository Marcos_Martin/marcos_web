class ContactNotifier < ActionMailer::Base
  def contact_me(message)
    @message=message
    file=message.file
    unless file.blank?
      attachments[file.original_filename] = File.open(file.path, 'rb'){|f| f.read}
    end
    mail to: "Marcos Martin<marcos.martingm@gmail.com>",
         subject: "[MarcosWeb]: "+message.subject, from: message.email
  end
end
