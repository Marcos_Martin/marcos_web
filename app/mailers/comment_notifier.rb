class CommentNotifier < ActionMailer::Base
  def contact_me(message)
    @message=message
    mail to: "Marcos Martin<marcos.martingm@gmail.com>",
         subject: "[MarcosWeb]: Nuevo comentario (#{message.subject.split('/').last})",
         from: message.email
  end
end