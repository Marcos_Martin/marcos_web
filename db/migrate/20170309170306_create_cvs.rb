class CreateCvs < ActiveRecord::Migration
  def change
    create_table :cvs do |t|
      t.text :body

      t.timestamps
    end
  end
end
