class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.text :body
      t.string :user
      t.string :email
      #replicas a los comentarios desactivados en modelo y migracion
      #t.integer :replied_of_id
      t.integer :commentable_id
      t.string :commentable_type
      t.string :slug

      t.timestamps
    end
  end
end
