class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :title
      t.text :body
      t.string :media
      t.boolean :draft, default: false
      t.string :slug

      t.timestamps
    end
  end
end
