class CreateProyectPhotos < ActiveRecord::Migration
  def change
    create_table :proyect_photos do |t|
      t.integer :proyect_id
      t.string :media
      t.string :slug
    end
    add_index :proyect_photos, :slug, unique: true
  end
end
