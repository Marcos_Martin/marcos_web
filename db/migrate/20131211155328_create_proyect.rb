class CreateProyect < ActiveRecord::Migration
  def change
    create_table :proyects do |t|
      t.string :name
      t.text :description
      t.string :sort_description
      t.string :languague
      t.string :media
      t.string :slug
      t.timestamps
    end
    add_index :proyects, :slug, unique: true
  end
end
