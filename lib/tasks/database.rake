namespace :database do
  desc "TODO"
  task :reset => :environment do
    Rake::Task["db:drop"].invoke
    p "--------------DROP OK"
    Rake::Task["db:create"].invoke
    p "--------------CREATE OK"
    Rake::Task["db:migrate"].invoke
    p "--------------MIGRATE OK"
    Rake::Task["db:seed"].invoke
    p "--------------SEED OK"
  end

  task :reset_and_seed_example => :environment do

  end

end
